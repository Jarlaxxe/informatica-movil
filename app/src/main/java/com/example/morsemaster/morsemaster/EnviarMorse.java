package com.example.morsemaster.morsemaster;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.String;

public class EnviarMorse extends AppCompatActivity {
    private Morse morse = new Morse();
    private Button btnEnviar;
    private Button buttonPunto;
    private Button buttonRaya;
    private Button buttonEspacio;
    private Button buttonBorrar;
    private Camera camera;
    private EditText enviarTexto;
    private MediaPlayer mediaPlayer;
    private RadioGroup rgOpciones;
    private boolean mensajeEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_morse);
        mediaPlayer = MediaPlayer.create(this, R.raw.beep);
        Context context = this;
        PackageManager pm = context.getPackageManager();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Log.e("err", "Device has no camera!");
            Toast.makeText(getApplicationContext(),
                    "Your device doesn't have camera!",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        mensajeEditText = false;
        enviarTexto = (EditText) findViewById(R.id.enviarTexto);
        enviarTexto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mensajeEditText) {
                    alertaDialogo("Aviso", "Para tu comodidad, recomendamos ocultar el teclado y utilizar los botones que hemos habilitado para escribir código morse. Aunque si prefieres utilizar el teclado puedes hacerlo...");
                    mensajeEditText = true;
                }
            }
        });

        btnEnviar = (Button) findViewById(R.id.btnEnviar);
        rgOpciones = (RadioGroup) findViewById(R.id.rgLinternaAudio);
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String texto = enviarTexto.getText().toString();
                if (!texto.equals("")) {
                    if (codigoValido(texto)) {
                        int idSeleccionado = rgOpciones.getCheckedRadioButtonId();
                        switch (idSeleccionado) {
                            case R.id.cbLinterna:
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    flashApiMayorQue23(texto);
                                } else {
                                    flashApiMenorQue22(texto);
                                }
                                break;
                            case R.id.cbAudio:
                                try {
                                    for (int i = 0; i < texto.length(); i++) {
                                        Character letra = texto.charAt(i);
                                        if (letra.equals('.')) {
                                            mediaPlayer.seekTo(600);
                                            mediaPlayer.start();
                                            Thread.sleep(400);
                                            mediaPlayer.pause();
                                            Thread.sleep(100);
                                        } else if (letra.equals('-')) {
                                            mediaPlayer.seekTo(400);
                                            mediaPlayer.start();
                                            Thread.sleep(600);
                                            mediaPlayer.pause();
                                            Thread.sleep(100);
                                        } else {//letra.equals espacio
                                            Thread.sleep(500);
                                        }
                                    }
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                break;
                        }
                    }
                    else{
                        alertaDialogo("ERROR", "Por favor introduce un código morse válido.");
                    }
                } else {
                    alertaDialogo("ERROR", "Por favor introduce un texto.");
                }
            }
        });

        buttonPunto = (Button) findViewById(R.id.btPunto);
        buttonPunto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarTexto.setText(enviarTexto.getText().toString() + ".");
                enviarTexto.setSelection(enviarTexto.getText().length()); //Para que el cursor siempre esté colocado detrás de la última letra
            }
        });

        buttonRaya = (Button) findViewById(R.id.btRaya);
        buttonRaya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarTexto.setText(enviarTexto.getText().toString() + "-");
                enviarTexto.setSelection(enviarTexto.getText().length()); //Para que el cursor siempre esté colocado detrás de la última letra
            }
        });

        buttonEspacio = (Button) findViewById(R.id.btEspacio);
        buttonEspacio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarTexto.setText(enviarTexto.getText().toString() + " ");
                enviarTexto.setSelection(enviarTexto.getText().length()); //Para que el cursor siempre esté colocado detrás de la última letra
            }
        });

        buttonBorrar = (Button) findViewById(R.id.btBorrar);
        buttonBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enviarTexto.getText().toString().length() > 0) {
                    enviarTexto.setText(enviarTexto.getText().toString().substring(0, enviarTexto.getText().toString().length() - 1));
                    enviarTexto.setSelection(enviarTexto.getText().length()); //Para que el cursor siempre esté colocado detrás de la última letra
                }
            }
        });

    }

    //Métodos para utilizar Camera y Camera2
    @SuppressWarnings("deprecation")
    private void flashApiMenorQue22(String texto) {
        camera = Camera.open();
        final Camera.Parameters p = camera.getParameters();
        try {
            for (int i = 0; i < texto.length(); i++) {
                Character letra = texto.charAt(i);
                if (letra.equals('.')) {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(p);
                    Thread.sleep(100);
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    camera.setParameters(p);
                } else if (letra.equals('-')) {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(p);
                    Thread.sleep(750);
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    camera.setParameters(p);
                } else {//letra.equals espacio
                    Thread.sleep(750);
                }
                Thread.sleep(250);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        camera.release();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void flashApiMayorQue23(String texto) {
        //FlashLightUtilForL flashLightUtilForL = new FlashLightUtilForL(context,this);
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            String cameraID = cameraManager.getCameraIdList()[0];
            for (int i = 0; i < texto.length(); i++) {
                Character letra = texto.charAt(i);
                if (letra.equals('.')) {
                    if (cameraManager.getCameraCharacteristics(cameraID).get(CameraCharacteristics.FLASH_INFO_AVAILABLE))
                        cameraManager.setTorchMode(cameraID, true);
                    Thread.sleep(100);
                    if (cameraManager.getCameraCharacteristics(cameraID).get(CameraCharacteristics.FLASH_INFO_AVAILABLE))
                        cameraManager.setTorchMode(cameraID, false);
                } else if (letra.equals('-')) {
                    if (cameraManager.getCameraCharacteristics(cameraID).get(CameraCharacteristics.FLASH_INFO_AVAILABLE))
                        cameraManager.setTorchMode(cameraID, true);
                    Thread.sleep(750);
                    if (cameraManager.getCameraCharacteristics(cameraID).get(CameraCharacteristics.FLASH_INFO_AVAILABLE))
                        cameraManager.setTorchMode(cameraID, false);
                } else {//letra.equals(' ')
                    Thread.sleep(750);
                }
                Thread.sleep(250);
            }
        } catch (InterruptedException | CameraAccessException e) {
            Log.e("FragmentFlashlight", "Failed to interact with camera.", e);
            Toast.makeText(this, "Torch Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }


    private void alertaDialogo(String titulo, String textoAlerta) {
        AlertDialog alertDialog = new AlertDialog.Builder(EnviarMorse.this).create();
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(textoAlerta);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public boolean codigoValido(String texto) {
        boolean valido = true;
        for (char c : texto.toCharArray()) {
            if (c == '.' || c == '-' || c == ' ') continue;
            else {
                valido = false;
                break;
            }
        }
        return valido;
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (camera != null) {
            camera.release();
        }
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true); //Muestra la flecha superior izquierda <-
        }
    }

    /*
    * Nos permite volver atrás pinchando en la flecha superior izquierda
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
