package com.example.morsemaster.morsemaster;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class FuncionesMorse extends AppCompatActivity {

    private Button enviarMorse;
    private Button traducirMorse;
    private Button recibirMorse;
    private Button practicarMorse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_funciones_morse);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        traducirMorse=(Button)findViewById(R.id.buttonTraductorMorse);
        //Implementamos el evento click del botón TRADUCIRMORSE
        traducirMorse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creamos el Intent
                Intent intent =
                        new Intent(FuncionesMorse.this, TraductorMorse.class);


                //Iniciamos la nueva actividad
                startActivity(intent);
            }
        });

        recibirMorse=(Button)findViewById(R.id.buttonRecibir);
        //Implementamos el evento click del botón LINTERNA
        recibirMorse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertaDialogo("Función No disponible", "Esta función estará disponible en la v2.0");
            }
        });


        enviarMorse=(Button)findViewById(R.id.buttonEnviar);
        //Implementamos el evento click del botón LINTERNA
        enviarMorse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creamos el Intent
                Intent intent = new Intent(FuncionesMorse.this, EnviarMorse.class);
                //Iniciamos la nueva actividad
                startActivity(intent);
            }
        });

        practicarMorse=(Button)findViewById(R.id.buttonPracticarMorse);
        //Implementamos el evento onclick del botón PRACTICAR
        practicarMorse.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Creamos el Intent
                Intent intent = new Intent(FuncionesMorse.this, PracticarMorse.class);
                //Iniciamos la nueva actividad
                startActivity(intent);
            }
        });
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true); //Muestra la flecha superior izquierda <-
        }
    }

    /*
    * Nos permite volver atrás pinchando en la flecha superior izquierda
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void alertaDialogo(String titulo, String textoAlerta) {
        AlertDialog alertDialog = new AlertDialog.Builder(FuncionesMorse.this).create();
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(textoAlerta);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
