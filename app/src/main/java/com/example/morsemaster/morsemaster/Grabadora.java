package com.example.morsemaster.morsemaster;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;


import com.musicg.api.*;
import com.musicg.main.demo.RenderSpectrogramDemo;
import com.musicg.wave.WaveHeader;

import java.nio.ByteBuffer;

public class Grabadora extends AppCompatActivity {

    boolean grabActi=false;
    private Toolbar appbar;
    Audio au1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grabadora);
        appbar = (Toolbar) findViewById(R.id.toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        final ToggleButton grabadoraSwitch = (ToggleButton) findViewById(R.id.toggleGrabador);

        RenderSpectrogramDemo spectro=new RenderSpectrogramDemo();

        //Asignamos el manejador de evento click para que, cada vez que se pulse el botón, ejecutemos el hilo de grabación/escucha
        grabadoraSwitch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if(grabActi==false){
                    //Ejecución del hilo de grabación/escucha
                    grabActi=true;
                    au1=new Audio();
                    //RenderSpectrogramDemo spectro;
                    //ClapApi cpa=new ClapApi(new WaveHeader());
                    //cpa.isClap(null);

                }
                else{
                    au1.stop();
                    au1.close(); //Paramos la grabación/escucha
                    grabActi=false;

                }
            }
        });





    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true); //Muestra la flecha superior izquierda <-
        }
    }

    /*
    * Nos permite volver atrás pinchando en la flecha superior izquierda
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
 * Thread to manage live recording/playback of voice input from the device's microphone.
 */
    private class Audio extends Thread
    {
        private boolean stopped = false;

        /**
         * Give the thread high priority so that it's not canceled unexpectedly, and start it
         */
        private Audio()
        {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
            start();
        }

        @Override
        public void run()
        {
            Log.i("Audio", "Running Audio Thread");
            AudioRecord recorder = null;
            AudioTrack track = null;
            short[][]   buffers  = new short[256][160];
            int ix = 0;
            ClapApi cpa=new ClapApi(new WaveHeader());
        /*
         * Initialize buffer to hold continuously recorded audio data, start recording, and start playback.
         */
            try
            {
                //int N = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_IN_DEFAULT, AudioFormat.ENCODING_PCM_16BIT);
                int N=getValidSampleRates();
                //Log.i("Buffer Audio", "La comprobación devuelve: " + N);
                recorder = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, 8000, AudioFormat.CHANNEL_IN_STEREO, AudioFormat.ENCODING_PCM_16BIT, N*10);
                track = new AudioTrack(AudioManager.STREAM_MUSIC, 8000,
                        AudioFormat.CHANNEL_IN_STEREO, AudioFormat.ENCODING_PCM_16BIT, N*10, AudioTrack.MODE_STREAM);
                recorder.startRecording();
                track.play();
            /*
             * Loops until something outside of this thread stops it.
             * Reads the data from the recorder and writes it to the audio track for playback.
             */
                while(!stopped)
                {
                    Log.i("Map", "Writing new data to buffer");
                    short[] buffer = buffers[ix++ % buffers.length];
                    N = recorder.read(buffer,0,buffer.length);
                    track.write(buffer, 0, buffer.length);
                    
                    //java.nio.ByteBuffer bb = java.nio.ByteBuffer.allocate(buffer.length * 2);
                    //bb.asShortBuffer().put(buffer);
                    //return bb.array(); // this returns the "raw" array, it's shared and not copied!
                    ByteBuffer buf = java.nio.ByteBuffer.allocate(buffer.length * 2);
                    byte[] arr = new byte[buf.remaining()];
                    buf.get(arr);
                    Log.i("Map", "Ha pillado un aplauso: " + cpa.isClap(arr));
                }
            }
            catch(Throwable x)
            {
                Log.w("Audio", "Error reading voice audio", x);
            }
        /*
         * Frees the thread's resources after the loop completes so that it can be run again
         */
            finally
            {
                recorder.stop();
                recorder.release();
                track.stop();
                track.release();
            }
        }

        /**
         * Called from outside of the thread in order to stop the recording/playback loop
         */
        private void close()
        {
            stopped = true;
        }

        public int getValidSampleRates() {
            int bufferSize=0;
            int retorno=0;
            for (int rate : new int[] {4000, 5000, 6000, 7000, 8000, 11025, 16000, 22050, 44100}) {  // add the rates you wish to check against
                bufferSize = AudioRecord.getMinBufferSize(rate, AudioFormat.CHANNEL_CONFIGURATION_DEFAULT, AudioFormat.ENCODING_PCM_16BIT);
                if (bufferSize > 0) {
                    // buffer size is valid, Sample rate supported
                    retorno=bufferSize;
                }
            }
            return retorno;
        }
    }
}
