
package com.example.morsemaster.morsemaster;

import android.hardware.Camera;
import android.hardware.camera2.CameraDevice;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Linterna extends AppCompatActivity {

    private CameraCaptureSession mSession;
    private CaptureRequest.Builder mBuilder;
    private CameraDevice mCameraDevice;
    private boolean flashEncendido=false;
    private boolean isFlashOn = false;
    private boolean isTimerOn=false;
    private Camera camera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linterna);
        final ToggleButton flashSwitch = (ToggleButton) findViewById(R.id.buttonFlashlight);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            flashSwitch.setChecked(false);
            flashSwitch.setOnCheckedChangeListener(new MyCheckedChangeListener());
        }
        else{
            //default of flash mode  is on
            camera = Camera.open();
            final Camera.Parameters p = camera.getParameters();

            flashSwitch.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    //Obtenemos el padre que tiene como fondo la imagen de la bombilla...
                    RelativeLayout padreBlanco=(RelativeLayout)findViewById(R.id.zonaBlanca);
                    enciendeApagaFlash(isFlashOn, p, flashSwitch);
                }
            });
        }
    }

    private CameraManager mCameraManager;

    /**
     * switch listener
     */
    class MyCheckedChangeListener implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            try {
                mCameraManager = (CameraManager) Linterna.this.getSystemService(Context.CAMERA_SERVICE);
                //here to judge if flash is available
                CameraCharacteristics cameraCharacteristics = mCameraManager.getCameraCharacteristics("0");
                boolean flashAvailable = cameraCharacteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
                if (flashAvailable) {
                    if(isChecked) { //Si está checked, significa que el flash debería estar encendido
                        mCameraManager.setTorchMode("0", false);
                    } else { //Si no, significa que el flash debería estar apagado
                       mCameraManager.setTorchMode("0", true);
                    }
                } else {
                    Toast.makeText(Linterna.this, "Flash not available", Toast.LENGTH_SHORT).show();
                    //todo: throw Exception
                }

            } catch (CameraAccessException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true); //Muestra la flecha superior izquierda <-
        }
    }

    /*
    * Nos permite volver atrás pinchando en la flecha superior izquierda
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void close() {
        if (mCameraDevice == null || mSession == null) {
            return;
        }
        mSession.close();
        mCameraDevice.close();
        mCameraDevice = null;
        mSession = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        close();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(mCameraDevice!=null)
                mCameraDevice.close();
        }
        else{
            if (camera != null) {
                camera.release();
            }
        }
    }

    /*
     Enciende o apaga el flash (si estaba apagado, lo enciende; si estaba encendido, lo apaga).
     */
    public void enciendeApagaFlash(boolean estadoFlash, Camera.Parameters p, ToggleButton flashSwitch){
        RelativeLayout padreBlanco=(RelativeLayout)findViewById(R.id.zonaBlanca);
        if (isFlashOn) {
            Log.i("info", "torch is turned off!");
            p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(p);
            isFlashOn = false;
            flashSwitch.setText("Torch-OFF");
            //Cambiamos el fondo (ponemos la bombilla apagada)
            padreBlanco.setBackgroundResource(R.drawable.lightbulboff);
        } else {
            Log.i("info", "torch is turned on!");
            p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(p);
            isFlashOn = true;
            flashSwitch.setText("Torch-ON");
            //Cambiamos el fondo (ponemos la bombilla encendida)
            padreBlanco.setBackgroundResource(R.drawable.lightbulb);
        }
    }
}
