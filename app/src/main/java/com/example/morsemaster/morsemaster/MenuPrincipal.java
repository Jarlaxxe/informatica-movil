package com.example.morsemaster.morsemaster;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

public class MenuPrincipal extends AppCompatActivity
         {
             private Toolbar appbar;
             private DrawerLayout drawerLayout;
             private NavigationView navView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        appbar = (Toolbar) findViewById(R.id.toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, appbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);

        navView = (NavigationView) findViewById(R.id.nav_view);

        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        Log.d("COCO", "Entra al método nuevo del onNavigationItemSelected");
                        // Handle navigation view item clicks here.
                        int id = menuItem.getItemId();
                        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

                        if (id == R.id.inicio) {

                        } else if (id == R.id.flash) {
                            Intent intent = new Intent(MenuPrincipal.this, Linterna.class);
                            startActivity(intent);
                        } else if (id == R.id.morse) {
                            Intent intent = new Intent(MenuPrincipal.this, FuncionesMorse.class);
                            startActivity(intent);
                        } else if (id == R.id.menu_ajustes) {
                            Intent intent = new Intent(MenuPrincipal.this, SettingsActivity.class);
                            startActivity(intent);
                        } else if (id == R.id.menu_ayuda) {
                            Intent intent = new Intent(MenuPrincipal.this, Ayuda2.class);
                            startActivity(intent);
                        } else if (id == R.id.menu_info) {
                            Intent intent = new Intent(MenuPrincipal.this, AcercaDe.class);
                            startActivity(intent);
                        }
                        else if (id == R.id.grabadora) {
                            Intent intent = new Intent(MenuPrincipal.this, Grabadora.class);
                            startActivity(intent);
                        }
                        else if (id == R.id.action_settings) {
                            Intent intent = new Intent(MenuPrincipal.this, SettingsActivity.class);
                            startActivity(intent);
                        }

                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                        return true;
                    }
                });

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_nav_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        ImageButton flash=(ImageButton)findViewById(R.id.Linterna);
        //Implementamos el evento click del botón LINTERNA
        flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(MenuPrincipal.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MenuPrincipal.this,
                            new String[]{Manifest.permission.CAMERA}, 1);
                } else {
                    Intent intent = new Intent(MenuPrincipal.this, Linterna.class);
                    startActivity(intent);
                }
            }
        });

        ImageButton morse=(ImageButton)findViewById(R.id.Morse);
        //Implementamos el evento click del botón TRADUCIRMORSE
        morse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creamos el Intent
                Intent intent = new Intent(MenuPrincipal.this, FuncionesMorse.class);
                //Iniciamos la nueva actividad
                startActivity(intent);
            }
        });

        //Implementamos el evento click del botón AJUSTES
        ImageButton ajustes=(ImageButton)findViewById(R.id.Ajustes);
        ajustes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creamos el Intent
                Intent intent = new Intent(MenuPrincipal.this, SettingsActivity.class);
                //Iniciamos la nueva actividad
                startActivity(intent);
            }
        });

        //Implementamos el evento click del botón AYUDA
        ImageButton ayuda=(ImageButton)findViewById(R.id.Ayuda);
        ayuda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creamos el Intent
                Intent intent = new Intent(MenuPrincipal.this, Ayuda2.class);
                //Iniciamos la nueva actividad
                startActivity(intent);
            }
        });

        //Implementamos el evento click del botón INFORMACION (Acerca de)
        ImageButton info=(ImageButton)findViewById(R.id.Informacion);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creamos el Intent
                Intent intent = new Intent(MenuPrincipal.this, AcercaDe.class);
                //Iniciamos la nueva actividad
                startActivity(intent);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: //CASE CAMERA_PERMISSION
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(true) {
                        Intent intent = new Intent(this, Linterna.class);
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(this, "Please grant camera permission to use the flashlight", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        DrawerLayout drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        //R.id.nav_send
        if (id == 16908332) {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                drawerLayout.openDrawer(GravityCompat.START);
            }
            return true;
        }

        else if (id == R.id.action_settings) {
            Intent intent = new Intent(MenuPrincipal.this, SettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}

