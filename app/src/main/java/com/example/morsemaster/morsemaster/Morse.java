package com.example.morsemaster.morsemaster;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by leyre on 09/03/2016.
 */
public class Morse {
    private Map<String,String> mapMorse;

    public Morse (){
        mapMorse = new HashMap<String,String>();
        mapMorse.put("a",".-");
        mapMorse.put("b","-...");
        mapMorse.put("c","-.-.");
        mapMorse.put("d","-..");
        mapMorse.put("e",".");
        mapMorse.put("f","..-.");
        mapMorse.put("g","--.");
        mapMorse.put("h","....");
        mapMorse.put("i","..");
        mapMorse.put("j",".---");
        mapMorse.put("k","-.-");
        mapMorse.put("l",".-..");
        mapMorse.put("m","--");
        mapMorse.put("n","-.");
        mapMorse.put("o","---");
        mapMorse.put("p",".--.");
        mapMorse.put("q","--.-");
        mapMorse.put("r",".-.");
        mapMorse.put("s","...");
        mapMorse.put("t","-");
        mapMorse.put("u","..-");
        mapMorse.put("v","...-");
        mapMorse.put("w",".--");
        mapMorse.put("x","-..-");
        mapMorse.put("y","-.--");
        mapMorse.put("z","--..");
        mapMorse.put("0","-----");
        mapMorse.put("1",".----");
        mapMorse.put("2","..---");
        mapMorse.put("3","...--");
        mapMorse.put("4","....-");
        mapMorse.put("5",".....");
        mapMorse.put("6","-....");
        mapMorse.put("7","--...");
        mapMorse.put("8","---..");
        mapMorse.put("9","----.");
    }
    //Devuelve el código morse(String) asignado a esa letra/número
    public String LetraAMorse(String letra){
        return mapMorse.get(letra);
    }

    //Devuelve un String con la traducción a morse de la palabra introducida
    public String PalabraAMorse (String palabra){
        String morseFinal=" ";
        for (int i=0;i<palabra.length();i++){
            if(i!=0){
                morseFinal+=" ";
            }
            if(mapMorse.get(String.valueOf(palabra.charAt(i)))!= null){
            morseFinal+= mapMorse.get(String.valueOf(palabra.charAt(i)));
            }
            else{
                morseFinal = "Error1#";
                break;
            }
        }
        return morseFinal;
    }

    //Devuelve la letra correspondiente al código morse introducido
    public String MorseALetra (String morse){
        String letra="";
        Iterator it = mapMorse.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry)it.next();
            if (morse.equals(e.getValue())){
                letra= e.getKey().toString();
                break;
            }
            else{
                letra= "El código morse introducido no se corresponde con ningun caracter";
            }
        }
        return letra;
    }

    //Devuelve la palabra correspondiente al código morse introducido, cada letra en morse separada por espacios.
    public String MorseAPalabra (String morse){
        String palabra="";
        String[] partesMorse = morse.split(" ");
        Iterator it = mapMorse.entrySet().iterator();
        while (it.hasNext()) {
            for(int i=0;i<partesMorse.length;i++){
                Map.Entry e = (Map.Entry)it.next();
                if (partesMorse[i].equals(e.getValue())){
                    palabra+= e.getKey().toString();
                }
            }
        }
        if (palabra!="")return palabra;
        else return "Error2#";
    }
}
