package com.example.morsemaster.morsemaster;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

@SuppressWarnings("deprecation")
public class PracticarMorse extends AppCompatActivity {

    private String[] palabrasAleatorias = {"casa", "hola", "cable", "morse", "coche", "rosa", "kiwi"};
    private static final int NUM_PALABRAS = 7;
    private Button buttonSonido;
    private Button buttonLuz;
    private Button buttonValidarPalabra;
    private Button buttonPunto;
    private Button buttonRaya;
    private Button buttonEspacio;
    private Button buttonBorrar;
    private Button buttonNuevaPalabra;
    private String morseAemitir;
    private EditText txtMorse;
    private TextView txtPalabraAleatoria;
    private TextView txtIntentos;
    private TextView txtCorrectos;
    private TextView txtFallidos;
    private int intentos;
    private int correctos;
    private int fallidos;
    private boolean validada;
    private MediaPlayer mediaPlayer;
    private Camera camera;
    private Morse m;
    private Boolean mensajeEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_practicar_morse);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);


        morseAemitir = "";
        txtIntentos = (TextView) findViewById(R.id.tvIntentos);
        txtCorrectos = (TextView) findViewById(R.id.tvCorrectos);
        txtFallidos = (TextView) findViewById(R.id.tvFallidos);
        txtPalabraAleatoria = (TextView) findViewById(R.id.palabraAleatoria);
        Random r = new Random();
        int indice = r.nextInt(NUM_PALABRAS);
        String palAleatoria = palabrasAleatorias[indice];
        txtPalabraAleatoria.setText(palAleatoria);
        intentos = 0;
        fallidos = 0;
        correctos = 0;
        validada = false;
        txtMorse = (EditText) findViewById(R.id.TxtEscrito);
        buttonSonido = (Button) findViewById(R.id.buttonSonido);
        mediaPlayer = MediaPlayer.create(this, R.raw.beep);
        mensajeEditText = false;
        txtMorse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mensajeEditText) {
                    alertaDialogo("Aviso","Para tu comodidad, recomendamos ocultar el teclado y utilizar los botones que hemos habilitado para escribir código morse. Aunque si prefieres utilizar el teclado puedes hacerlo...");
                    mensajeEditText = true;
                }
            }
        });

        buttonSonido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String texto = txtMorse.getText().toString();
                try {
                    if (!texto.equals("")) {
                        for (int i = 0; i < texto.length(); i++) {
                            Character letra = texto.charAt(i);
                            if (letra.equals('.')) {
                                mediaPlayer.seekTo(600);
                                mediaPlayer.start();
                                Thread.sleep(400);
                                mediaPlayer.pause();
                                Thread.sleep(100);
                            } else if (letra.equals('-')) {
                                mediaPlayer.seekTo(400);
                                mediaPlayer.start();
                                Thread.sleep(600);
                                mediaPlayer.pause();
                                Thread.sleep(100);
                            } else {//letra.equals espacio
                                Thread.sleep(500);
                            }
                        }
                    } else {
                        alertaDialogo("ERROR", "Por favor introduce un texto.");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        buttonLuz = (Button) findViewById(R.id.buttonLuz);
        buttonLuz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String texto = "";
                texto = txtMorse.getText().toString();
                if (!texto.equals("")) {
                    //flashApiMayorQue21(texto, context);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        flashApiMayorQue23(texto);
                    } else {
                        flashApiMenorQue22(texto);
                    }
                } else {
                    alertaDialogo("ERROR", "Por favor introduce un texto.");
                }
            }
        });

        buttonValidarPalabra = (Button) findViewById(R.id.btValidar);
        buttonValidarPalabra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m = new Morse();
                String textoMorse = "";
                String txtm = "";
                textoMorse = m.PalabraAMorse(txtPalabraAleatoria.getText().toString());
                textoMorse = textoMorse.substring(1);
                if(!validada) {
                    txtm = txtMorse.getText().toString();
                    if (textoMorse.equals(txtm)) {
                        alertaDialogo("CORRECTO", "¡Es correcto!");
                        buttonLuz.setEnabled(true);
                        buttonSonido.setEnabled(true);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            buttonLuz.setBackground(getResources().getDrawable(R.drawable.button_border_verde));
                            buttonSonido.setBackground(getResources().getDrawable(R.drawable.button_border_verde));
                        }
                        correctos++;
                        txtCorrectos.setText("Correctos:" + correctos);
                        morseAemitir = txtm;
                        validada = true;
                        //txtMorse.setText("");
                    } else {
                        alertaDialogo("ERROR", "El código morse introducido no es correcto. Vuelve a intentarlo");
                        buttonLuz.setEnabled(false);
                        buttonSonido.setEnabled(false);
                        fallidos++;
                        txtFallidos.setText("Fallidos:" + fallidos);
                    }
                    intentos++;
                    txtIntentos.setText("Intentos:" + intentos);
                }else{
                    alertaDialogo("AVISO", "La palabra ya ha sido validada");
                }
            }

        });

        buttonPunto = (Button) findViewById(R.id.btPunto);
        buttonPunto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtMorse.setText(txtMorse.getText().toString() + ".");
                txtMorse.setSelection(txtMorse.getText().length()); //Para que el cursor siempre esté colocado detrás de la última letra
            }
        });

        buttonRaya = (Button) findViewById(R.id.btRaya);
        buttonRaya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtMorse.setText(txtMorse.getText().toString() + "-");
                txtMorse.setSelection(txtMorse.getText().length()); //Para que el cursor siempre esté colocado detrás de la última letra
            }
        });

        buttonEspacio = (Button) findViewById(R.id.btEspacio);
        buttonEspacio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtMorse.setText(txtMorse.getText().toString() + " ");
                txtMorse.setSelection(txtMorse.getText().length()); //Para que el cursor siempre esté colocado detrás de la última letra
            }
        });

        buttonBorrar = (Button) findViewById(R.id.btBorrar);
        buttonBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtMorse.getText().toString().length() > 0) {
                    txtMorse.setText(txtMorse.getText().toString().substring(0, txtMorse.getText().toString().length() - 1));
                    txtMorse.setSelection(txtMorse.getText().length()); //Para que el cursor siempre esté colocado detrás de la última letra
                }
            }
        });

        buttonNuevaPalabra = (Button) findViewById(R.id.btNuevaPalabra);
        buttonNuevaPalabra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    buttonLuz.setBackground(getResources().getDrawable(R.drawable.button_border_rojo));
                    buttonSonido.setBackground(getResources().getDrawable(R.drawable.button_border_rojo));
                }
                buttonLuz.setEnabled(false);
                buttonSonido.setEnabled(false);
                Random r = new Random();
                int indice = r.nextInt(NUM_PALABRAS);
                String palAleatoria = palabrasAleatorias[indice];
                txtPalabraAleatoria.setText(palAleatoria);
                txtMorse.setText("");
                validada = false;
            }
        });

    }// Fin onCreate

    //Métodos para utilizar Camera y Camera2
    @SuppressWarnings("deprecation")
    private void flashApiMenorQue22(String texto) {
        camera = Camera.open();
        final Camera.Parameters p = camera.getParameters();
        try {
            for (int i = 0; i < texto.length(); i++) {
                Character letra = texto.charAt(i);
                if (letra.equals('.')) {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(p);
                    Thread.sleep(100);
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    camera.setParameters(p);
                } else if (letra.equals('-')) {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(p);
                    Thread.sleep(750);
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    camera.setParameters(p);
                } else {//letra.equals espacio
                    Thread.sleep(750);
                }
                Thread.sleep(250);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        camera.release();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void flashApiMayorQue23(String texto) {
        //FlashLightUtilForL flashLightUtilForL = new FlashLightUtilForL(context,this);
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            String cameraID = cameraManager.getCameraIdList()[0];
            for (int i = 0; i < texto.length(); i++) {
                Character letra = texto.charAt(i);
                if (letra.equals('.')) {
                    if (cameraManager.getCameraCharacteristics(cameraID).get(CameraCharacteristics.FLASH_INFO_AVAILABLE))
                        cameraManager.setTorchMode(cameraID, true);
                    Thread.sleep(100);
                    if (cameraManager.getCameraCharacteristics(cameraID).get(CameraCharacteristics.FLASH_INFO_AVAILABLE))
                        cameraManager.setTorchMode(cameraID, false);
                } else if (letra.equals('-')) {
                    if (cameraManager.getCameraCharacteristics(cameraID).get(CameraCharacteristics.FLASH_INFO_AVAILABLE))
                        cameraManager.setTorchMode(cameraID, true);
                    Thread.sleep(750);
                    if (cameraManager.getCameraCharacteristics(cameraID).get(CameraCharacteristics.FLASH_INFO_AVAILABLE))
                        cameraManager.setTorchMode(cameraID, false);
                } else {//letra.equals(' ')
                    Thread.sleep(750);
                }
                Thread.sleep(250);
            }
        } catch (InterruptedException | CameraAccessException e) {
            Log.e("FragmentFlashlight", "Failed to interact with camera.", e);
            Toast.makeText(this, "Torch Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (camera != null) {
            camera.release();
        }
    }

    private void alertaDialogo(String titulo, String textoAlerta) {
        AlertDialog alertDialog = new AlertDialog.Builder(PracticarMorse.this).create();
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(textoAlerta);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true); //Muestra la flecha superior izquierda <-
        }
    }

    /*
    * Nos permite volver atrás pinchando en la flecha superior izquierda
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
