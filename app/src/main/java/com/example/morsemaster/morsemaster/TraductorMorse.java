package com.example.morsemaster.morsemaster;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;
import android.hardware.Camera.Parameters;

import java.util.HashMap;
import java.util.Locale;


@SuppressWarnings("deprecation")
public class TraductorMorse extends AppCompatActivity {
    private Morse morse = new Morse();
    private Button traducir;
    private EditText textoTraducir;
    private EditText textoTraducido;
    private RadioGroup rgOpciones;
    private TextView txt1;
    private TextView txt2;
    private Button btnFlash;
    private Button btnSonido;
    private Button btnDictado;
    private Button buttonPunto;
    private Button buttonRaya;
    private Button buttonEspacio;
    private Button buttonBorrar;
    private Camera camera;
    private MediaPlayer mediaPlayer;
    private TextToSpeech textToSpeech;
    private LinearLayout botones;
    private boolean mensajeEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traductor_morse);
        final Context context = this;
        PackageManager pm = context.getPackageManager();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        botones = (LinearLayout) findViewById(R.id.layoutBotones);
        botones.setVisibility(View.GONE);

        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Log.e("err", "Device has no camera!");
            Toast.makeText(getApplicationContext(),
                    "Your device doesn't have camera!",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        rgOpciones = (RadioGroup) findViewById(R.id.GrbOpcionesTraduccion);
        rgOpciones.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                txt1 = (TextView) findViewById(R.id.Txt1);
                txt2 = (TextView) findViewById(R.id.Txt2);
                switch (checkedId) {
                    case R.id.txtAMorse:
                        txt1.setText("Escribe el texto");
                        txt2.setText("Morse");
                        botones.setVisibility(View.GONE);
                        break;
                    case R.id.MorseATxt:
                        botones.setVisibility(View.VISIBLE);
                        txt1.setText("Escribe el código morse");
                        txt2.setText("Texto");
                        break;
                }
            }
        });

        traducir = (Button) findViewById(R.id.btTraducir);
        textoTraducir = (EditText) findViewById(R.id.TxtEscrito);
        textoTraducido = (EditText) findViewById(R.id.TxtTraducido);
        mensajeEditText = false;
        textoTraducir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mensajeEditText && (rgOpciones.getCheckedRadioButtonId() == R.id.MorseATxt)) {
                    alertaDialogo("Aviso","Para tu comodidad, recomendamos ocultar el teclado y utilizar los botones que hemos habilitado para escribir código morse. Aunque si prefieres utilizar el teclado puedes hacerlo...");
                    mensajeEditText = true;
                }
            }
        });

        textoTraducir.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textoTraducido.setText("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //Implementamos el evento click del botón de TRADUCIR
        traducir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!textoTraducir.getText().toString().equals("")) {
                    String traducido = "";
                    Spannable trad;
                    String texto = textoTraducir.getText().toString();
                    String[] palabras = texto.split(" ");
                    //COMPROBAR QUE CHECK ESTA MARCADO PARA LLAMAR A UN MÉTODO O A OTRO
                    int idSeleccionado = rgOpciones.getCheckedRadioButtonId();
                    textoTraducido.setText("");
                    switch (idSeleccionado) {
                        case R.id.txtAMorse:

                            for (int i = 0; i < palabras.length; i++) {
                                //traducido += morse.PalabraAMorse(palabras[i].toLowerCase());

                                trad = new SpannableString(morse.PalabraAMorse(palabras[i].toLowerCase()));
                                if (i % 2 == 0) {
                                    trad.setSpan(new ForegroundColorSpan(Color.BLUE), 0, morse.PalabraAMorse(palabras[i].toLowerCase()).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                } else {
                                    trad.setSpan(new ForegroundColorSpan(Color.GREEN), 0, morse.PalabraAMorse(palabras[i].toLowerCase()).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                }

                                //trad.setSpan(new ForegroundColorSpan(Color.BLUE), 0, morse.PalabraAMorse(palabras[i].toLowerCase()).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                                textoTraducido.append(trad);


                            }
                            break;
                        case R.id.MorseATxt:
                            for (int i = 0; i < palabras.length; i++) {
                                //traducido += morse.PalabraAMorse(palabras[i].toLowerCase());
                                trad = new SpannableString(morse.MorseAPalabra(palabras[i].toLowerCase()));
                                if (i % 2 == 0) {
                                    trad.setSpan(new ForegroundColorSpan(Color.BLUE), 0, morse.MorseAPalabra(palabras[i].toLowerCase()).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                } else {
                                    trad.setSpan(new ForegroundColorSpan(Color.GREEN), 0, morse.MorseAPalabra(palabras[i].toLowerCase()).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                }

                                //trad.setSpan(new ForegroundColorSpan(Color.BLUE), 0, morse.PalabraAMorse(palabras[i].toLowerCase()).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                                textoTraducido.append(trad);

                            }
                            break;
                    }
                    if (textoTraducido.getText().toString().contains("Error2#")) {
                        alertaDialogo("Error","Has introducido una porción de código morse que no se corresponde con ninguna letra del alfabeto o número del 1 al 9. Por favor revisa el código y vuelve a pulsar 'Traducir'.");
                        textoTraducido.setText("");
                    } else if (textoTraducido.getText().toString().contains("Error1#")) {
                        alertaDialogo("Error","Has introducido algún caracter que no tiene traducción a código morse. Valores válidos [Aa-Zz][1-9]. Por favor revisa el código y vuelve a pulsar 'Traducir'.");
                        textoTraducido.setText("");
                    } else {
                        textoTraducido.setTextSize(30);
                        textoTraducido.setTextColor(Color.BLACK);
                    }
                    //textoTraducido.setText(traducido);
                }else{
                    alertaDialogo("Aviso","Por favor introduzca un texto.");
                }
            }
        });

        /* * * * * * * * * * * * *
        *   INCIO EMITIR MORSE   *
        * * * * * * * * * * * * */

        //Emitir morse Flash
        btnFlash = (Button) findViewById(R.id.emitirFlash);
        btnFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String texto = "";
                texto = getTextoEditTextEmitirMorse(rgOpciones, textoTraducido, textoTraducir);
                if (!texto.equals("")) {
                    //flashApiMayorQue21(texto, context);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        flashApiMayorQue23(texto);
                    } else {
                        flashApiMenorQue22(texto);
                    }
                } else {
                    //Mostrar un alert con error diciéndole que introduzca texto
                    alertaDialogo("Aviso","Por favor introduzca un texto.");
                }
            }
        });

        //Emitir morse Altavoz
        btnSonido = (Button) findViewById(R.id.emitirSonido);
        mediaPlayer = MediaPlayer.create(this, R.raw.beep);
        btnSonido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String texto = getTextoEditTextEmitirMorse(rgOpciones, textoTraducido, textoTraducir);
                try {
                    if (!texto.equals("")) {
                        for (int i = 0; i < texto.length(); i++) {
                            Character letra = texto.charAt(i);
                            if (letra.equals('.')) {
                                mediaPlayer.seekTo(600);
                                mediaPlayer.start();
                                Thread.sleep(400);
                                mediaPlayer.pause();
                                Thread.sleep(100);
                            } else if (letra.equals('-')) {
                                mediaPlayer.seekTo(400);
                                mediaPlayer.start();
                                Thread.sleep(600);
                                mediaPlayer.pause();
                                Thread.sleep(100);
                            } else {//letra.equals espacio
                                Thread.sleep(500);
                            }
                        }
                    } else {
                        //Mostrar un alert diciendole que introduzca un texto
                        alertaDialogo("Aviso","Por favor introduzca un texto.");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        /* * * * * * * * * * * * *
        *    FIN EMITIR MORSE    *
        * * * * * * * * * * * * */


        /* * * * * * * * * * * * *
        * INCIO DICTADO POR VOZ  *
        * * * * * * * * * * * * */
        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(new Locale("spa", "ESP"));
                }
            }
        });
        btnDictado = (Button) findViewById(R.id.dictadoVoz);
        btnDictado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    //Recojo el texto a dictar del EditText correspondiendente
                    String texto = getTextoEditTextDictadoVoz(rgOpciones, textoTraducido, textoTraducir);
                    if(!texto.equals("")){
                        //Llamo a la librería TextToSpeech dependiendo de la versión de Android que está ejecutando la aplicación
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            ttsGreater21(texto);
                        } else {
                            ttsUnder20(texto);
                        }
                    }else{
                        alertaDialogo("Aviso","No hay nada que dictar. Por favor traduce un texto a código morse o viceversa y vuelve a pulsar en el botón 'Dictado voz'.");
                    }
                }
        });
        /* * * * * * * * * * * * *
        *  FIN DICTADO POR VOZ   *
        * * * * * * * * * * * * */

        buttonPunto = (Button) findViewById(R.id.btPunto);
        buttonPunto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textoTraducir.setText(textoTraducir.getText().toString() + ".");
                textoTraducir.setSelection(textoTraducir.getText().length()); //Para que el cursor siempre esté colocado detrás de la última letra
            }
        });

        buttonRaya = (Button) findViewById(R.id.btRaya);
        buttonRaya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textoTraducir.setText(textoTraducir.getText().toString() + "-");
                textoTraducir.setSelection(textoTraducir.getText().length()); //Para que el cursor siempre esté colocado detrás de la última letra
            }
        });

        buttonEspacio = (Button) findViewById(R.id.btEspacio);
        buttonEspacio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textoTraducir.setText(textoTraducir.getText().toString() + " ");
                textoTraducir.setSelection(textoTraducir.getText().length()); //Para que el cursor siempre esté colocado detrás de la última letra
            }
        });

        buttonBorrar = (Button) findViewById(R.id.btBorrar);
        buttonBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textoTraducir.getText().toString().length() > 0) {
                    textoTraducir.setText(textoTraducir.getText().toString().substring(0, textoTraducir.getText().toString().length() - 1));
                    textoTraducir.setSelection(textoTraducir.getText().length()); //Para que el cursor siempre esté colocado detrás de la última letra
                }
            }
        });
    }

    /*
    * Detecta en cuál de los dos EditText está situado el texto en morse que hay que emitir
    * @param (RadioGroup)
    * @param (EditText)
    * @param (EditText)
    * @return (String)
     */
    private String getTextoEditTextEmitirMorse(RadioGroup rgOpciones, EditText textoTraducido, EditText textoTraducir) {
        String texto = "";
        int idSeleccionado = rgOpciones.getCheckedRadioButtonId();
        switch (idSeleccionado) {
            case R.id.txtAMorse:
                texto = textoTraducido.getText().toString();
                break;
            case R.id.MorseATxt:
                texto = textoTraducir.getText().toString();
                break;
        }
        return texto;
    }

    /*
    * Detecta en cuál de los dos EditText está situado el texto que hay que dictar
    * @param (RadioGroup)
    * @param (EditText)
    * @param (EditText)
    * @return (String)
     */
    private String getTextoEditTextDictadoVoz(RadioGroup rgOpciones, EditText textoTraducido, EditText textoTraducir) {
        String texto = "";
        int idSeleccionado = rgOpciones.getCheckedRadioButtonId();
        switch (idSeleccionado) {
            case R.id.txtAMorse:
                texto = textoTraducir.getText().toString();
                break;
            case R.id.MorseATxt:
                texto = textoTraducido.getText().toString();
                break;
        }
        return texto;
    }

    //Métodos para utilizar Camera y Camera2
    @SuppressWarnings("deprecation")
    private void flashApiMenorQue22(String texto) {
        camera = Camera.open();
        final Parameters p = camera.getParameters();
        try {
            for (int i = 0; i < texto.length(); i++) {
                Character letra = texto.charAt(i);
                if (letra.equals('.')) {
                    p.setFlashMode(Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(p);
                    Thread.sleep(100);
                    p.setFlashMode(Parameters.FLASH_MODE_OFF);
                    camera.setParameters(p);
                } else if (letra.equals('-')) {
                    p.setFlashMode(Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(p);
                    Thread.sleep(750);
                    p.setFlashMode(Parameters.FLASH_MODE_OFF);
                    camera.setParameters(p);
                } else {//letra.equals espacio
                    Thread.sleep(750);
                }
                Thread.sleep(250);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        camera.release();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void flashApiMayorQue23(String texto /*, Context context*/) {
        //FlashLightUtilForL flashLightUtilForL = new FlashLightUtilForL(context,this);
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            String cameraID = cameraManager.getCameraIdList()[0];
            for (int i = 0; i < texto.length(); i++) {
                Character letra = texto.charAt(i);
                if (letra.equals('.')) {
                    if (cameraManager.getCameraCharacteristics(cameraID).get(CameraCharacteristics.FLASH_INFO_AVAILABLE))
                        cameraManager.setTorchMode(cameraID, true);
                    Thread.sleep(100);
                    if (cameraManager.getCameraCharacteristics(cameraID).get(CameraCharacteristics.FLASH_INFO_AVAILABLE))
                        cameraManager.setTorchMode(cameraID, false);
                } else if (letra.equals('-')) {
                    if (cameraManager.getCameraCharacteristics(cameraID).get(CameraCharacteristics.FLASH_INFO_AVAILABLE))
                        cameraManager.setTorchMode(cameraID, true);
                    Thread.sleep(750);
                    if (cameraManager.getCameraCharacteristics(cameraID).get(CameraCharacteristics.FLASH_INFO_AVAILABLE))
                        cameraManager.setTorchMode(cameraID, false);
                } else {//letra.equals(' ')
                    Thread.sleep(750);
                }
                Thread.sleep(250);
            }
        } catch (InterruptedException | CameraAccessException e) {
            Log.e("FragmentFlashlight", "Failed to interact with camera.", e);
            Toast.makeText(this, "Torch Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
    //Fin Métodos para utilizar Camera y Camera2


    //Métodos para utilizar TextToSpeech => Dictado por voz
    @SuppressWarnings("deprecation")
    private void ttsUnder20(String text) {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, map);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void ttsGreater21(String text) {
        String utteranceId = this.hashCode() + "";
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
    }
    //Fin TextToSpeech


    @Override
    protected void onStop() {
        super.onStop();

        if (camera != null) {
            camera.release();
        }
    }

    private void alertaDialogo(String title, String textoAlerta) {
        AlertDialog alertDialog = new AlertDialog.Builder(TraductorMorse.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(textoAlerta);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true); //Muestra la flecha superior izquierda <-
        }
    }

    /*
    * Nos permite volver atrás pinchando en la flecha superior izquierda
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
