package com.example.morsemaster.morsemaster;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.preference.Preference;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.media.AudioManager;
import android.app.Activity;

/**
 * Created by Jarlaxxe on 19/03/2016.
 */
public class VolumePreference extends Preference{

    /*
    Progreso por defecto
     */
    private static final int DEFAULT_PROGRESS = 75;
    private AudioManager audioManager=null;
    /*
    Progreso actual
     */
    private int currentProgress;

    public VolumePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        currentProgress = DEFAULT_PROGRESS;
    }

    @Override
    protected View onCreateView(ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)
                getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        audioManager=(AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.STREAM_MUSIC);
        return inflater.inflate(R.layout.preference_volumen, parent, false);
    }

    @Override
    protected void onBindView(final View view) {

        // Setear el progreso actual a la seekbar
        SeekBar seekBar = (SeekBar)view.findViewById(R.id.selector);
        seekBar.setProgress(currentProgress);

        // Setear la escucha al seekbar
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                currentProgress = progress;
                ImageView imageView = (ImageView) view.findViewById(R.id.icono);
                if(currentProgress==0)
                    imageView.setImageResource(R.drawable.ic_lock_silent_mode);
                else
                    imageView.setImageResource(R.drawable.ic_lock_silent_mode_off);
                persistInt(currentProgress);
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // No es necesario
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // No es necesario
            }
        });
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInt(index, DEFAULT_PROGRESS);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {

        if (restorePersistedValue) {
            // Obtener el progreso actual
            currentProgress = this.getPersistedInt(DEFAULT_PROGRESS);
            //Aquí debería acceder al icono y cambiarlo en función del valor del ajuste
            //¿Problema? No tengo ni pajolera idea de cómo hacerlo con esos parámetros del método actual
/*          ImageView imageView = (ImageView) view.findViewById(R.id.icono);
            if(currentProgress==0)
                imageView.setImageResource(R.drawable.ic_lock_silent_mode);
            else
                imageView.setImageResource(R.drawable.ic_lock_silent_mode_off);
            persistInt(currentProgress);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                    progress, 0);*/
        } else {
            // Reiniciar al valor por defecto
            currentProgress = (Integer) defaultValue;
            persistInt(currentProgress);
            //Aquí debería acceder al icono y cambiarlo en función del valor del ajuste
            //¿Problema? No tengo ni pajolera idea de cómo hacerlo con esos parámetros del método actual
        }
    }


}
